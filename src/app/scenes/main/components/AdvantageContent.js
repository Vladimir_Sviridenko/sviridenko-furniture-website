import React from 'react';

import './styles/AdvantageContent.css';
import './styles/AdvantageContentMedia.css';

import {AdvantageElement} from './AdvantageElement';

export function AdvantageContent(props) {

    return (
        <div className='advantage-content'>

            {props.elements.map( (item) => 
            <AdvantageElement
                key={item.id}
                icon={item.icon}
                title={item.title}
                description={item.description}
            />)}

        </div>
    );
}