import React from 'react';

import './styles/AdvantageElement.css';
import './styles/AdvantageElementMedia.css';

export function AdvantageElement(props) {

    return (
        <div className='advantage-item'>
            <div className='advantage-item__title'>
                <img className='advantage-item__icon' src={props.icon} alt=''></img>
                {props.title}
            </div>
            <div className='advantage-item__description'>{props.description}</div>
        </div>
    );
}