import React from 'react';

import './styles/PartnersContent.css';
import './styles/PartnersContentMedia.css';


export function PartnersContent() {

    const partnersLogoLinks = [
        { 
            link: 'assets/main/partners-logos/egger-logo.png', 
            alt: 'Egger Logo' 
        },
        { 
            link: 'assets/main/partners-logos/kronospan-logo.png', 
            alt: 'Kronospan Logo' 
        },
        { 
            link: 'assets/main/partners-logos/blum-logo.png', 
            alt: 'Blum Logo' 
        },
        { 
            link: 'assets/main/partners-logos/kdm-logo.svg', 
            alt: 'Kdm Logo' 
        },
        { 
            link: 'assets/main/partners-logos/kedr-logo.png', 
            alt: 'Kedr Logo' 
        },
        { 
            link: 'assets/main/partners-logos/market_glass-logo.png', 
            alt: 'Market-glass Logo' 
        },
    ];

    return (
        <div className='partners'>
            <div className='partners__title-container'>
                <span className='partners__title'>НАШИ ПАРТНЁРЫ</span>
            </div>
            <div className='partners__logo-container'>
                {partnersLogoLinks.map( (img) => 
                    <img key={img.alt} className='partners-logo' src={img.link} alt={img.alt}></img> 
                )}
            </div>
        </div>
    )
}