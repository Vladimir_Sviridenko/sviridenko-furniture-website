import React, {memo} from 'react';

import { AdvantageContent } from './components/AdvantageContent';
import { PartnersContent } from './components/PartnersContent';

import './styles/main.css';
import './styles/mainMedia.css';
import { useLoadingImagesTimeout } from '../../hooks/useLoadingImagesTimeout';

const aboutStyle = {
    backgroundImage: `url('assets/main/main.jpg')`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
};

export const Main = memo(() => {

    useLoadingImagesTimeout('.main', 'img');

    const about = ["МЕБЕЛЬ ЛЮБОЙ СЛОЖНОСТИ", "МОСКВА И МОСКОВСКАЯ ОБЛАСТЬ"];

    const arrAdvantageElements = [
        {
            id: 0,
            icon: 'assets/main/advantages-icons/any_difficult.png',
            title: 'ЛЮБАЯ СЛОЖНОСТЬ',
            description:
                `Кухни, шкафы-купе, комоды, столы, полки, 
                гардеробные, зеркала, межкомнатные двери...
                Сделаем всё, что угдоно вашей душе :) 
                Можете ознакомиться с тем что мы уже сделали.
                На сайте и в группе ВК.`
        },
        {
            id: 1,
            icon: 'assets/main/advantages-icons/high_quality.png',
            title: 'ВЫСОКОЕ КАЧЕСТВО',
            description:
                `Наши партнёры поставляют нам высококачественную 
                продукцию. Вы получите высокое качество 
                как комплектующих так и сборки, по приемлимой цене.`
                
        },
        {
            id: 2,
            icon: 'assets/main/advantages-icons/freeline.png',
            title: 'БЕСПЛАТНЫЙ ЗАМЕР',
            description:
                `Вам бесплатно предоставляется замер.
                В замер входит полная консультация о 
                будущей постройке, устанавливается стоймость,
                выбор цвета покрытия из большого набора образцов от наших партнёров.`
                
        },
    ];
    return (
        <div className='main' style={{opacity: 0}}>
            <div className='about' style={aboutStyle}>
                <div className='about__text'>{about[0]}<br />{about[1]}</div>
            </div>
            <AdvantageContent elements={arrAdvantageElements} />
            <PartnersContent className='footer'></PartnersContent>
        </div>
    )
})