import React, {memo} from 'react';

import './styles/gallery.css';
import './styles/galleryMedia.css';

import { useLoadingImagesTimeout } from '../../hooks/useLoadingImagesTimeout';
import { useLazyLoader } from '../../hooks/useLazyLoader';

export const Gallery = memo( () => {

    let images = createArrayOfImages(18);
    
    useLazyLoader('.gallery__image_lazy');
    useLoadingImagesTimeout('.gallery', '.gallery__image');

    function imageToFullScreen(e) {
        if (!e.currentTarget.classList.contains('gallery__image_full-screen')) {
            e.currentTarget.classList.add('gallery__image_full-screen');

            const imageWrap = e.currentTarget.parentNode;
            imageWrap.style.backgroundImage = `url('${e.currentTarget.dataset.src}')`;
            imageWrap.style.backgroundRepeat = 'no-repeat';
            imageWrap.style.backgroundSize = 'cover';
            imageWrap.style.backgroundPosition = 'center';
        } else {
            e.currentTarget.classList.remove('gallery__image_full-screen');
        }
    }

    function createArrayOfImages(imagesAmoung) {
        const images = [];

        for (let i = 1; i <= imagesAmoung; i++) {
            images.push(
                <div key={i - 1} className='gallery__image-container'>
                    <img className='gallery__image gallery__image_lazy' onClick={imageToFullScreen} data-src={'assets/portfolio/gallery/img (' + i + ').jpg'} alt=''></img>
                </div>
            );
        };
        return (images);
    };

    return (
        <div className='gallery' style={{opacity: 0}}>
            {images}
        </div>
    );
})