import Milling from './classes/Milling';

export default class MillingsDb {
    constructor() {
        this._millings = [
            new Milling('01', 'Монолит'),
            new Milling('02', 'Зигзаг'),
            new Milling('03', 'Квадро'),
            new Milling('04', 'Выборка'),
            new Milling('05', 'Порту'),
            new Milling('06', 'Змейка')
        ]
    }

    getDefaultMilling() {
        return this._millings[0];
    }
    
    getMillingById(id) {
        for(let milling of this._millings) {
            if(milling._id === id) {
                return milling;
            }
        }
    }
}