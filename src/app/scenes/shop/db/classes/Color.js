export default class Color {
    constructor(id, name) {
        this._id = id;
        this._name = name;
        this._categorySetted = false;
    }

    getId() {
        return this._id;
    }
    
    getName() {
        return this._name;
    }

    setCategory(category) {
        if(!this._categorySetted) {
            this._category = category;
            this._categorySetted = true;
        }
    }

    getCategory() {
        return this._category;
    }

    getImageUrl() {
        let url = `/assets/shop/kitchen_cabinets/colors/${this._category}/${this._id}.jpg`
        return url;
    }
    
    static toColor({id, name, category}){
        const color = new Color(id, name);
        color.setCategory(category);
        return color;
    }
}
