export default class KitchenСabinet {
    constructor(id, name, size, price) {
        this._id = id;
        this._name = name;
        this._size = size;
        this._price = price;

        
        this._quantity = 1;
        this._color = '3Т';
        this._milling = 'Стандарт';
    }

    getImageUrl() {
        let url = `assets/shop/kitchen_cabinets/${this._id}.jpg`;
        return url;
    }

    getId() {
        return this._id;
    }

    getName() {
        return this._name;
    }

    getPrice() {
        return this._price;
    }

    getSize() {
        return this._size;
    }

    getColor() {
        return this._color;
    }

    getMilling() {
        return this._milling;
    }

    setQuantity(quantity) {
        if(quantity > 0) {
            this._quantity = quantity;
        } 
    }

    getQuantity() {
        return this._quantity;
    }

    equalToCartItem(cartItem) {
        if( this._id === cartItem.id &&
            this._color === cartItem.color &&
            this._miling === cartItem.miling) {
                return true;
        } else {
            return false;
        }
    }

    toCartItem() {
        return {
            id: this._id, 
            color: this._color, 
            milling: this._milling, 
            quantity: this._quantity
        };
    }

    setColor(color) {
        this._color = color;
    }

    setMilling(millingId) {
        this._milling = millingId;
    }
}