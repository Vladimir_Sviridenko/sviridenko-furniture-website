export default class Milling {
    constructor(id, name) {
        this._id = id;
        this._name = name;
    }

    getId() {
        return this._id;
    }
    
    getName() {
        return this._name;
    }

    getImageUrl() {
        const url = `assets/shop/kitchen_cabinets/millings/${this._id}.jpg`;
        return url;
    }
}