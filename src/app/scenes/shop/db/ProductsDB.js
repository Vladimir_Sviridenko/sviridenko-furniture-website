import KitchenCabinet from './classes/KitchenCabinet'

export default class ProductsDB {
    
    constructor() {
        this._products = [
            new KitchenCabinet( '001','Шкаф навесной','72×40×34', 2192),
            new KitchenCabinet( '002', 'Шкаф навесной', '72×50×34', 2400),
            new KitchenCabinet( '003', 'Шкаф навесной', '72×60×34', 2575),
            new KitchenCabinet( '004', 'Шкаф навесной', '72×80×34', 2764),
            new KitchenCabinet( '005', 'Шкаф навесной угловой', '72×61×61', 3163),
            new KitchenCabinet( '006', 'Шкаф навесной низкий', '36×60×34', 1868),
            new KitchenCabinet( '007', 'Шкаф навесной низкий', '36×80×34', 2278),
    
            new KitchenCabinet( '008', 'Шкаф напольный', '82×40×53', 2405),
            new KitchenCabinet( '009', 'Шкаф напольный (2 ящика)', '82×40×53', 3884),
            new KitchenCabinet( '010', 'Шкаф напольный (3 ящика)', '82×40×53', 4325),
            new KitchenCabinet( '011', 'Шкаф напольный', '82×50×53', 2775),
            new KitchenCabinet( '012', 'Шкаф напольный', '82×60×53', 3248),
            new KitchenCabinet( '013', 'Шкаф напольный (1 ящик)', '82×60×53', 4060),
            new KitchenCabinet( '014', 'Шкаф напольный под духовку (1 ящик)', '82×60×53', 3189),
            new KitchenCabinet( '015', 'Шкаф напольный', '82×80×53', 3504),
            new KitchenCabinet( '016', 'Шкаф напольный угловой', '82×91×53', 4068),
            new KitchenCabinet( '017', 'Шкаф-пенал под духовку', '214×60×56', 5564),
            new KitchenCabinet( '018', 'Шкаф-пенал', '214×60×56', 7473)
        ];
    }

    getProducts() {
        return this._products;
    }

    getProductById(id) {
        for(let product of this._products) {
            if(product._id === id) {
                return new KitchenCabinet(product._id, product._name, product._size, product._price);
            }
        }
        return null;
    }
}