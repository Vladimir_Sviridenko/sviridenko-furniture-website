import Color from './classes/Color';

export default class ColorsDB {
    constructor() {
        this._colorGroups = [
            {
                name: 'Древесные матовые',
                categories: {
                    1: [
                        new Color('3T', 'Белое дерево'),
                        new Color('1441', 'Белый распил'),
                        new Color('2584-2', 'Венге серебро'),
                        new Color('2584-1', 'Венге золото'),
                        new Color('1012', 'Распил перламутр'),
                        new Color('11014', 'Ива светлая'),
                        new Color('241TP', 'Дуб белфорд'),
                        new Color('8195', 'Берёза'),
                        new Color('9132', 'Ясень натуральный'),
                        new Color('5028', 'Дуб светлый'),
                        new Color('631TK', 'Ларче светлый'),
                        new Color('512TE', 'Дуб сонома светлый'),
                        new Color('194-7T', 'Венге светлый'),
                        new Color('203-6T', 'Ель карпатская'),
                        new Color('13T', 'Дуб молодой'),
                        new Color('14083', 'Серое дерево'),
                        new Color('9111', 'Орех перламутр'),
                        new Color('513TE', 'Дуб сонома тёмный'),
                        new Color('9156', 'Дерево торнадо'),
                        new Color('246-2T', 'Дуб антик'),
                        new Color('246T', 'Старое дерево'),
                        new Color('5029', 'Дуб тёмный'),
                        new Color('9301', 'Зебрано горизонт'),
                        new Color('11015', 'Ива тёмная'),
                        new Color('9346-6', 'Венге шоколад'),
                        new Color('9345-6', 'Венге табакко'),
                    ],
                    2: [
                        new Color('473', 'Риф белоснежный'),
                    ],
                    3: [
                        new Color('9187-1', 'Лиственица сибирская'),
                        new Color('9187-5', 'Лиственица камчасткая'),
                        new Color('9187-3', 'Лиственница приморская'),
                        new Color('9187-6', 'Лиственица амурская'),
                        new Color('011', 'Томан')
                    ],
                    4: []
                }
            },
            {
                name: 'Древесные патинированные',
                categories: {
                    1: [],
                    2: [
                        new Color('5005', 'Роялвуд белый'),
                        new Color('5009', 'Роялвуд жемчуг'),
                        new Color('5098', 'Роялвуд крем'),
                        new Color('5010', 'Роялвуд кофе'),
                        new Color('5011', 'Роялвуд серый'),
                        new Color('5092', 'Роялвуд бордо'),
                        new Color('5095', 'Роялвуд графит'),
                        new Color('54', 'Патина премиум'),
                        new Color('5457', 'Патина ясень серебро'),
                        new Color('85', 'Патина ясень золото')
                    ],
                    3: [
                        new Color('6059', 'Патина ясень 27'),
                        new Color('7539', 'Патина фисташка')
                    ],
                    4: []
                }
            }
        ];

        for (let group of this._colorGroups) {
            for (let category in group.categories) {
                for (let color of group.categories[category]) {
                    color.setCategory(category);
                }
            }
        }
    }

    getDefaultColor() {
        return this._colorGroups[0].categories[1][0];
    }

    getGroupNames() {
        let groupNames = [];

        for (let group of this._colorGroups) {
            groupNames.push(group.name);
        }

        return groupNames;
    }
    
    getColorsByGroupName(groupName) {
        let colors = [];
        for (let group of this._colorGroups) {
            if (group.name === groupName) {
                for (let category in group.categories) {
                    for (let color of group.categories[category]) {
                        colors.push(color);
                    }
                }
            }
        }
        return colors;
    }

    getColorById(id) {
        for(let groupName of this.getGroupNames()) {
            for(let color of this.getColorsByGroupName(groupName)){
                if(color.getId() === id) {
                    return color;
                }
            }
        }
        return false;
    }
}