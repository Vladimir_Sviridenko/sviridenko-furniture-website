import React, { memo } from 'react';

import './styles/CartItem.css';
import './styles/CartItemMedia.css';

export const CartItem = memo( (props) => {
    const item = props.item;
    const addProductToCart = props.addProductToCart;
    const removeProductFromCart = props.removeProductFromCart;
    
    return (
        <div className='cart-item'>
            <div className='cart-item__name'>{item.getName()} {item.getSize()}</div>

            <div className='cart-item__modificators'>
                <div className='cart-item__modificator' type='milling'><img src='assets/shop/icons/milling.png' alt='milling'></img>{item.getMilling()}</div>
                <div className='cart-item__modificator' type='color'><img src='assets/shop/icons/color.png' alt='color'></img>{item.getColor()}</div>
            </div>

            <div className='cart-item__price'>
                <div className='quantity'>
                    <img className='quantity__button' src='assets/shop/icons/remove-one.png' onClick={() => removeProductFromCart(item)} alt='minus'></img>
                        <span className='quantity__number'>{item.getQuantity()}</span>
                    <img className='quantity__button' src='assets/shop/icons/add-one.png' onClick={() => addProductToCart(item)} alt='plus'></img>
                </div>
                <div className='price__sum'>{item.getPrice() * item.getQuantity()}</div>
            </div>
        </div>
    );
})