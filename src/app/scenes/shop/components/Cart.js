import React, { memo } from 'react';

import './styles/Cart.css';
import './styles/CartMedia.css';

import { CartSwitch } from './CartSwitch';
import { CartItem } from './CartItem';

export const Cart = memo( (props) => {

    const productsDb = props.productsDb;
    const addProductToCart = props.addProductToCart;
    const removeProductFromCart = props.removeProductFromCart;
    const cartItems = props.cartItems;

    const getCartProducts = (cartItems) => {
        const cartProducts = [];
        for(let cartItem of cartItems) {
            const cartProduct = productsDb.getProductById(cartItem.id);
            if(cartProduct) {
                //Todo: add check on isExistingColor isExistingMilling, when add color and milling selectors
                cartProduct.setColor(cartItem.color);
                cartProduct.setMilling(cartItem.milling);
                cartProduct.setQuantity(cartItem.quantity);
                cartProducts.unshift(
                    <CartItem 
                    key={cartItem.id + cartItem.color + cartItem.milling} 
                    item={cartProduct} 
                    addProductToCart={addProductToCart} 
                    removeProductFromCart={removeProductFromCart}/>
                );
                cartProductsQuantity += cartProduct.getQuantity();
                cartSumPrice += cartProductsQuantity * cartProduct.getPrice();
            }
        }
        return cartProducts;
    }

    let cartProductsQuantity = 0;
    let cartSumPrice = 0;

    const cartProducts = getCartProducts(cartItems);
    
    return (
        <div className='cart-container cart-container_closed'>
            <CartSwitch productsQuantity={cartProductsQuantity} sumPrice={cartSumPrice} />
            <div className='cart cart_closed'>
                {cartProducts}
            </div>
        </div>
    )
})
