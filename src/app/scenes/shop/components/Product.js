import React, { memo } from 'react';

import './styles/Product.css';
import './styles/ProductMedia.css';

export const Product = memo((props) => {
    const product = props.product;
    const addProductToCart = props.addProductToCart;

    return (
        <div className='product'>
            <img className='product__image' src={product.getImageUrl()} alt=''></img>
            <div className='product__name'>{product.getName(20)} </div>
            <div className='product__size'>{product.getSize()}</div>
            <div className='product__price'>{product.getPrice()}</div>
            <div className='product__button' type='add' onClick={() => addProductToCart(product)}><img src='assets/shop/icons/plus.png' alt='+'></img></div>
        </div>
    );
})