import React, { memo } from 'react';

import { ModColorButton } from './ModButtonColor';
import { ModMillingButton } from './ModButtonMilling';

import './styles/ModBar.css';

export const ModBar = memo( (props) => {

    return(
        <div className='mod-bar'>
            <ModColorButton colorsDb={props.colorsDb} setColorId={props.setColorId} colorId={props.colorId}/>
            <ModMillingButton millingsDb={props.millingsDb} setMillingId={props.setMillingId} millingId={props.millingId}/>  
        </div>
    )
})
