import React, { memo } from 'react';
import { Link } from "react-router-dom";

export const ModMillingButton = memo((props) => {

    const millingsDb = props.millingsDb;
    const setMillingId = props.setMillingId;
    const millingId = props.millingId;

    const getChoosenMilling = () => {
        const milling = millingsDb.getMillingById(millingId);
        if(milling) {
            return milling;
        } else {
            return millingsDb.getDefaultMilling();
        }
    }

    const choosenMilling = getChoosenMilling();


    let modIconName;
    let pathname;
    if(window.location.pathname === '/shop/milling') {
        modIconName = 'arrow-back';
        pathname = '/shop';
    } else {
        modIconName = 'milling'
        pathname = '/shop/milling';
    }

    return (
        <Link to= {{pathname: pathname, setMillingId: setMillingId, millingDb: millingsDb }} className='mod-button'>
            <img className='mod-button__icon' src={`/assets/shop/icons/${modIconName}.png`} alt={modIconName}></img>
            <div className='mod-button__value'>{choosenMilling.getName()}</div>
        </Link>
    )
})
