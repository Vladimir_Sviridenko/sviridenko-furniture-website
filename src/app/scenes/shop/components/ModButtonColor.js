import React, { memo } from 'react';
import { Link } from "react-router-dom";


export const ModColorButton = memo( (props) => {

    const colorsDb = props.colorsDb;
    const setColorId = props.setColorId;
    const colorId = props.colorId;

    const getChoosenColor = () => {
        const color = colorsDb.getColorById(colorId);
        if(color) {
            return color;
        } else {
            return colorsDb.getDefaultColor();
        }
    }

    const choosenColor = getChoosenColor();

    let modIconName;
    let pathname;
    if(window.location.pathname === '/shop/colors') {
        modIconName = 'arrow-back';
        pathname = '/shop';
    } else {
        modIconName = 'color'
        pathname = '/shop/colors';
    }


    return(
            <Link to= {{pathname: pathname, setColorId: setColorId, colorsDb: colorsDb }} className='mod-button'>
                <img className='mod-button__icon' src={`/assets/shop/icons/${modIconName}.png`} alt={modIconName}></img>

                <div className='mod-button__value'>
                    <div className='color-mini__name'>{choosenColor.getId()}</div>
                    <img className='color-mini__icon' src={choosenColor.getImageUrl()} alt='color-icon'></img>
                </div>
        </Link>
    )
})
