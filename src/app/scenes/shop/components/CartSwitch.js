import React, { memo } from 'react';

import './styles/CartSwitch.css';
import './styles/CartSwitchMedia.css';

export const CartSwitch = memo( (props) => {

    function switchCart(e) {

        const cartContainer = e.currentTarget.parentElement;
        const switcherImage = document.querySelector('.cart-switch__arrow');
        const cart = document.querySelector('.cart');

        const closingElement = (window.innerWidth > 768) ? cart : cartContainer;

        if(closingElement.classList.contains(`${closingElement.classList[0]}_closed`)) {
            closingElement.classList.remove(`${closingElement.classList[0]}_closed`);
            switcherImage.classList.remove('cart-switch__arrow_closed');
        } else {
            closingElement.classList.add(`${closingElement.classList[0]}_closed`);
            switcherImage.classList.add('cart-switch__arrow_closed');
        }
    }

    return (
        <div className='cart-switch' onClick={switchCart}>
            <div className='cart-switch__quantity'>
                <img src='assets/shop/icons/cart.png' alt='cart'></img> 
                <span className='quantity__number'>{props.productsQuantity}</span>
            </div>
            <div className='cart-switch__sum-price'>{props.sumPrice}</div>
            <img className='cart-switch__arrow cart-switch__arrow_closed' src='assets/shop/icons/arrow_up.png' alt='arrow'></img>
        </div>
    );
})
