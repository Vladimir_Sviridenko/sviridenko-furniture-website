import React, { memo } from 'react';

import './styles/ProductsTable.css';
import './styles/ProductsTableMedia.css';

import { useUpdatingProductsModificators } from '../hooks/useUpdatingProductsModificators';
import { useLoadingImagesTimeout } from '../../../hooks/useLoadingImagesTimeout';
import { useScrollIntoView } from '../hooks/useScrollIntoView';

import { Product } from './Product';


export const ProductsTable = memo( (props) => {

    const products = props.products;
    const colorId = props.colorId;
    const millingId = props.millingId;
    const addProductToCart = props.addProductToCart;

    useScrollIntoView('.app');
    useLoadingImagesTimeout('.products-table', '.product__image');

    useUpdatingProductsModificators(products, colorId, millingId);

    return (
        <div className='products-table' style={{opacity: 0}}>
            {products.map((product) => <Product key={product.getId()} product={product} addProductToCart={addProductToCart}/>)}
        </div>
    )
})
