import React, { memo } from 'react';
import { Switch, Route } from "react-router-dom";

import './styles/Shop.css';
import './styles/ShopMedia.css';

import { useLocalStorage } from './hooks/useLocalStorage';

import { Cart } from './components/Cart';
import { ProductsTable } from './components/ProductsTable';
import { ModBar } from './components/ModBar';
import { ColorsTable } from './scenes/colorsTable/ColorsTable';
import { MillingsTable } from './scenes/millingsTable/MillingsTable';

import ProductsDB from './db/ProductsDB';
import ColorsDB from './db/ColorsDB';
import MillingsDb from './db/MillingsDb';

export const Shop = memo( () => {


    const productsDb = new ProductsDB();
    const colorsDb = new ColorsDB();
    const millingsDb = new MillingsDb();

    const products = productsDb.getProducts();

    const [cartItems, setCartItems] = useLocalStorage('cart', []);
    //TODO: add check on cart is iterable
    const [colorId, setColorId] = useLocalStorage('colorId', colorsDb.getDefaultColor().getId());
    const [millingId, setMillingId] = useLocalStorage('millingId', millingsDb.getDefaultMilling().getId());

    const addProductToCart = (product) => {
        const newCartItems = cartItems.concat();

        let cartHasProduct = false;
        for (let cItem of newCartItems) {
            if(product.equalToCartItem(cItem)) {
                cartHasProduct = true;
                cItem.quantity++;
                break;
            }
        }

        if(cartHasProduct) {
            setCartItems(newCartItems);
        } else {
            const newCartItem = product.toCartItem();
            setCartItems(cartItems.concat(newCartItem));
        }
    }

    const removeProductFromCart = (product) => {
        const newCartItems = cartItems.concat();

        let cartHasProduct = false;
        for (let i = 0; i < newCartItems.length; i++) {

            if(product.equalToCartItem(newCartItems[i])) {
                cartHasProduct = true;
                if(newCartItems[i].quantity > 1) {
                    newCartItems[i].quantity--;
                } else if(newCartItems[i].quantity === 1){
                    newCartItems.splice(i, 1);
                }
                break;
            }
        }

        if(cartHasProduct) {
            setCartItems(newCartItems);
        } else {
            const newCartItem = product.toCartItem();
            setCartItems(cartItems.concat(newCartItem));
        }
    }

    return (
        <div className='shop'>
            <ModBar
                colorsDb={colorsDb} setColorId={setColorId} colorId={colorId}
                millingsDb={millingsDb} setMillingId={setMillingId} millingId={millingId}
            />

            <Switch>
                <Route path='/shop/colors' component={ColorsTable}></Route>
                <Route path='/shop/milling' component={MillingsTable}></Route>
                <Route>
                    <Cart productsDb={productsDb} cartItems={cartItems} addProductToCart={addProductToCart} removeProductFromCart={removeProductFromCart}/>
                    <ProductsTable products={products.concat()} colorId={colorId} millingId={millingId} addProductToCart={addProductToCart}/>
                </Route>
            </Switch>

        </div>
    );
})
