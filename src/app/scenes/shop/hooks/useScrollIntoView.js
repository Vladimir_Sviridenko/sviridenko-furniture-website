import { useEffect } from "react";

export function useScrollIntoView(selector) {

    useEffect( () => {
        const element = document.querySelector(selector);

        if(element)
            element.scrollIntoView();
        else
            throw new Error(`be sure that element with class='${selector}' exists`);
    }, [selector]);
}
