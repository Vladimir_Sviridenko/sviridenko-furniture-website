import { useEffect } from "react";

export function useUpdatingProductsModificators(products, colorId, millingId) {
    useEffect( () => {
        for(let key in products) {
            products[key].setColor(colorId);
            products[key].setMilling(millingId);
        }
    })
}