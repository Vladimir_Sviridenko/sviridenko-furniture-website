import React, { memo } from 'react';

import './styles/Color.css';
import './styles/ColorMedia.css';

import { useLazyLoader} from '../../../../../hooks/useLazyLoader';

export const Color = memo( (props) => {

    const color = props.color;
    const storageColorId = props.storageColorId;

    useLazyLoader('.color__image_lazy');

    return (
        <div className='color' onClick={() => storageColorId(color.getId())} >
            <img className='color__image color__image_lazy' data-src={color.getImageUrl()} alt='color'></img>
            <div className='color__title'>
                {color.getId() + ' ' + color.getName() + ' ' + color.getCategory()}
            </div>
        </div>
    )
})
