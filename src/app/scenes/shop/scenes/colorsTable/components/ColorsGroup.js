import React, { memo } from 'react';

import './styles/ColorsGroup.css';
import './styles/ColorsGroupMedia.css';

import { Color } from './Color';

export const ColorsGroup = memo( (props) => {

    const groupName = props.groupName;
    const colors = props.colors;

    return (
        <div className='colors-group'>
            <div className='colors-group__title'>{groupName}</div>
            <div className='colors-group__colors-container'>
                {colors.map( (color) => <Color key={color.getId()} color={color} storageColorId={props.storageColorId}/>)}
            </div>
        </div>
    )
})
