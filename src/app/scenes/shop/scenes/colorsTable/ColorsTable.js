import React, { memo } from 'react';

import './styles/ColorsTable.css';
import './styles/ColorsTableMedia.css';

import { useLocalStorage } from '../../hooks/useLocalStorage';
import { ColorsGroup } from './components/ColorsGroup';

import ColorsDB from '../../db/ColorsDB';

import { useLoadingImagesTimeout } from '../../../../hooks/useLoadingImagesTimeout';
import { useScrollIntoView } from '../../hooks/useScrollIntoView';

export const ColorsTable = memo( (props) => {

    useScrollIntoView('.app');
    useLoadingImagesTimeout('.colors-table', '.color__image');

    let colorsDb = props.location.colorsDb;
    if(!colorsDb) colorsDb = new ColorsDB();

    const groupNames = colorsDb.getGroupNames();
    
    let storageColorId = props.location.setColorId;
    if(!storageColorId) storageColorId = useLocalStorage('colorId', colorsDb.getDefaultColor())[1];

    return (
        <div className='colors-table' style={{opacity: 0}}>
            {
                groupNames.map( (groupName) => 
                <ColorsGroup key={groupName} groupName={groupName} colors={colorsDb.getColorsByGroupName(groupName)} storageColorId={storageColorId}/>)
            }
        </div>
    )
})
