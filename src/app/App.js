import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import "./AppMedia.css";

import { NavBar } from "./components/NavBar";
import { Main } from "./scenes/main/Main";
import { Shop } from "./scenes/shop/Shop";
import { Gallery } from "./scenes/gallery/Gallery";
import { Contacts } from "./components/Contacts";

export function App() {
  const [isContactsOpened, setIsContactsOpened] = useState(false);

  function handleContactsClick() {
    setIsContactsOpened(!isContactsOpened);
  };

  return (
    <div className="app">
      <Router>
        <NavBar handleContactsClick={handleContactsClick} />
        <Contacts isContactsOpened={isContactsOpened} />
        <Switch >
          <Route exact path="/" component={Main}></Route>
          <Route path="/shop" component={Shop}></Route>
          <Route path="/works" component={Gallery}></Route>
        </Switch>
      </Router>
    </div>
  );
}