import { useEffect } from 'react';

export function useLoadingImagesTimeout(containerSelector, imageSelector) { 
 
    useEffect(() => {
        let container = getContainerElement(containerSelector);
        
        if(container.style.opacity !== '0') {
            throw new Error('be sure that component uses useLoadingImagesTimeout has style={{opacity: 0}}');
        }
        loadFirstVisibleImages(container);
        
        function getContainerElement(containerSelector){
            const container = document.querySelector(containerSelector);
            if (!(container instanceof HTMLDivElement)) {
                throw new Error(`The element of class ${containerSelector} is not a HTMLDivElement. Make sure a <div class="${containerSelector}"> element exists in the document.`);
            }
            return container;
        }

        function showLoader() {
            const loadImage = document.createElement('img');
            loadImage.classList.add('scene__loader');
            loadImage.src = '/assets/loading.gif';
            container.before(loadImage);
        }
    
        function loadFirstVisibleImages() {
            /*document.body.style.overflow = 'hidden';*/
            container.style.pointerEvents = 'none';

            let onTimeoutToLong = setTimeout(showLoader, 200);

            startLoadingTimeout().then( () => { 
                finishLoading()
                window.clearTimeout(onTimeoutToLong);
            });
        }

        function isFirstVisible(element) {
            if( element.getBoundingClientRect().bottom > 0 && element.getBoundingClientRect().bottom < window.innerHeight ) 
                return true;
            else
                return false;
        }
    
        async function startLoadingTimeout(){
            let promises = [];
            const firstImages = Array.from(container.querySelectorAll(imageSelector));
            for(let i = 0; i < firstImages.length; i++) {
                if(!isFirstVisible(firstImages[i])) break;
                let promise = new Promise((resolve, reject) => {
                    firstImages[i].addEventListener('load', function(){
                        resolve(true);
                    }, {once: true});
                    firstImages[i].addEventListener('error', function(){
                        reject(new Error("Image load error!"));
                    }, {once: true});
                })
                promises.push(promise);
            }
            return Promise.all(promises);
        }
    
        function finishLoading() {
            const loaderImage = document.querySelector(".scene__loader");
            if(loaderImage) {
                loaderImage.parentNode.removeChild(loaderImage);
            }
            container.style.opacity = 1;
            container.style.pointerEvents = 'auto';
            document.body.style.overflowY = 'scroll';
        }

    }, [containerSelector, imageSelector]);

}