import { useEffect } from 'react';

export function useLazyLoader(lazyImageSelector, isContainerLazy = false) {

    useEffect(() => {
        enableImageLazyLoader(lazyImageSelector, isContainerLazy);
    }, [lazyImageSelector, isContainerLazy]);

    function enableImageLazyLoader(lazyImageSelector, isContainerLazy) {

        let lazyloadImages = [];

        lazyloadImages = document.querySelectorAll(lazyImageSelector);
        lazyloadImages.forEach((image) => {
            if(isContainerLazy) {
                image.parentElement.style.opacity = 0;
            } else {
                image.style.opacity = 0;
            }
            image.addEventListener('load', (e) => {
                if(isContainerLazy) {
                    e.currentTarget.parentElement.style.opacity = 1;
                } else {
                    e.currentTarget.style.opacity = 1;
                }

            })
        });

        const imageObserver = new IntersectionObserver(entries => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    const image = entry.target;
                    image.setAttribute('src', image.dataset.src);
                    imageObserver.unobserve(image);
                    image.classList.remove(lazyImageSelector);
                }
            });
        });

        lazyloadImages.forEach(function (image) {
            imageObserver.observe(image);
        });

    }

}
