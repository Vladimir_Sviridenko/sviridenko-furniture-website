import React from "react";

import "./styles/Contacts.css";
import "./styles/ContactsMedia.css";

import { useDeleteTabIndexes } from './hooks/useDeleteTabIndexes';

export function Contacts(props) {

  useDeleteTabIndexes();

  return (
    <div className="contacts-container" hidden={!props.isContactsOpened}>
      <div className='contacts'>
        <div className="contacts__phone">
          <img src="/assets/contacts/phone-icon.png" alt="phone-icon" />
          <div>
            <a href="tel:+79256118364">+7 925 611-83-64</a>
            <br />
            <a href="tel:+79060932758">+7 906 093-27-58</a>
          </div>
        </div>
        <a className="contacts__email" href="email:n.n.lis@yandex.ru">
          <img src="/assets/contacts/email-icon.png" alt="email-icon" />
          n.n.lis@yandex.ru
          </a>
        <a className="contacts__whatsapp" href="https://wa.me/79256118364" target="_blank" rel="noopener noreferrer">
          WhatsApp:
            <img src="/assets/contacts/whatsapp-icon.png" alt="whatsapp-icon" />
        </a>
        <a className="contacts__vk" href="https://vk.com/sviridenko_furniture" target="_blank" rel="noopener noreferrer">
          Vk:<img src="/assets/contacts/vk-icon.png" alt="vk-icon " />
        </a>
      </div>
    </div>
  );
}
