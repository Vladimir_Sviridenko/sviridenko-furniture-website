import React from 'react';
import { Link, NavLink } from 'react-router-dom';

import './styles/NavBar.css';
import './styles/NavBarMedia.css';

export function NavBar(props) {

    const logoName = "SVIRIDENKO FURNITURE";

    return (
        <nav className="menu">
            <NavLink className="menu__logo" to="/">{logoName}</NavLink>
            <div className="menu__container">
                <NavLink onClick={props.handleShopClick} className="menu__nav-link" to="/shop" title = 'В РАЗРАБОТКЕ'>МАГАЗИН</NavLink>
                <NavLink className="menu__nav-link" to="/works">НАШИ РАБОТЫ</NavLink>
                <Link onClick={props.handleContactsClick} to='#' className="menu__nav-link">КОНТАКТЫ</Link>
            </div>
        </nav>
    )
}
