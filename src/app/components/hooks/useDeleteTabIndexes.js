import { useEffect } from 'react';

export function useDeleteTabIndexes() {
    useEffect(() => {
        deleteTabIndexes();
    }, []);

    function deleteTabIndexes() {
        const links = document.body
            .getElementsByClassName("contacts")[0]
            .getElementsByTagName("a");

        for (let i = 0; i < links.length; i++) {
            links[i].tabIndex = "-1";
        }
    }
}